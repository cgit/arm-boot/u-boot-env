video_args_hdmi=setenv video_args $video_args video=mxcfb${fb}:dev=hdmi,1280x720M@60,if=RGB24
dvimode=1024x768MR-16@60
defaultdisplay=dvi
ipaddr=192.168.2.151
serverip=192.168.2.172
setup_ip=192.168.2.151:192.168.2.172:192.168.2.1:255.255.255.0:S35:eth0:off:192.168.2.220:192.168.2.1
mmcargs=run video_args_script; setenv bootargs console=${console},${baudrate} root=${mmcroot} omapfb.mode=dvi:${dvimode} omapdss.def_disp=${defaultdisplay}
netargs=setenv bootargs console=${console},${baudrate}n8 root=/dev/nfs rw ip=${setup_ip} nfsroot=${serverip}:${rootnfs},v3,tcp omapfb.mode=dvi:${dvimode} omapdss.def_disp=${defaultdisplay}
bootfile=zImage-isee-linux-4.9.81-r0.3
fdtfile=omap3-igep-base0030-test.dtb
mmcpart=1
mmcroot=/dev/mmcblk0p2 rw rootwait
#bootdir=/boot/
rootnfs=/opt/nfs/ubuntu-xenial-armhf-base-test-igep-0.1-5
netload=tftpboot ${loadaddr} ${bootfile}; tftpboot ${fdtaddr} ${fdtfile}
loadbootenv=ext4load mmc ${mmcdev}:${mmcpart} ${loadaddr} ${bootdir}${bootenv}
loadfdt=ext4load mmc ${mmcdev}:${mmcpart} ${fdt_addr} ${bootdir}${fdt_file}
loadimage=ext4load mmc ${mmcdev}:${mmcpart} ${loadaddr} ${bootdir}${image}
ethaddr=0e:00:00:00:00:ff
